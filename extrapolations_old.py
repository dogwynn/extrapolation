import fnmatch
import json
import os
import re
import pprint
from collections import Counter, defaultdict
                 


def value(line):
    line = line.strip()
    line = line.split()
    value = float(line[len(line)-1])

    return value

def ehf(line):
    returned_value = None
    if re.search(r'!RHF STATE 1\.1 ENERGY',line):
        returned_value = value(line)

    elif re.search(r'!RHF STATE 1\.1 Energy',line):
        returned_value = value(line)

    return returned_value

def emp2(line):
    returned_value = None
    if re.search(r'MP2 STATE 1\.1 ENERGY',line):
        returned_value = value(line)

    elif re.search(r'!MP2 total energy',line):
        returned_value = value(line)

    elif re.search(r'!MP2 Energy',line):
        returned_value = value(line)

    elif re.search(r'MP2 total energy',line):
        returned_value = value(line) 

    return returned_value


def eccsd(line):
    returned_value = None
    if re.search(r'!CCSD STATE 1\.1 ENERGY',line):
        returned_value = value(line)
        
    elif re.search(r'CCSD total energy',line):
        returned_value = value(line)
        
    elif re.search(r'CCSD Energy',line):
        returned_value = value(line)
        
    return returned_value

def eccsdt(line):
    returned_value = None
    if re.search(r'!CCSD\(T\)',line):
        returned_value = value(line)
        
    return returned_value

#METHODS = {'hf','mp2','ccsd','ccsdt'}


def get_energy(data, dimer, config, dist, method, basis):
    try:
        energy = data[dimer][config][dist][method][basis]
    except KeyError:
        print('dimer',dimer,'config',config,'dist',dist,'method',method,
              'basis',basis)
        pprint.pprint(data[dimer][config][dist])
        raise 
    return energy

def method_energies(data, dimer, config, dist, basis):
    def energies(*methods):
        method_energies = [get_energy(data, dimer, config, dist, method, basis)
                            for method in methods]
        return method_energies
    return energies

def basis_energies(data, dimer, config, dist, method):
    def energies(*bases):
        return [get_energy(data, dimer, config, dist, method, basis)
                for basis in bases]
        #print(energies)
    return energies

def corr_energy(data, dimer, config, dist, basis):
    corr_energy = method_energies(data, dimer, config, dist, basis)
    hf, mp2, ccsd, ccsdt = corr_energy('hf', 'mp2', 'ccsd', 'ccsdt')
    mp2_corr = mp2 - hf
    ccsd_corr = ccsd - hf
    ccsdt_corr = ccsdt - hf
    return (mp2_corr, ccsd_corr, ccsdt_corr)

def feller(data, dimer, config, dist, method):
    if dimer == 'acet' or dimer == 'diac':
        if method == 'hf':
            energies = basis_energies(data, dimer, config, dist, method)
            ha6, ha5, haq = energies('ha6z', 'ha5z', 'haqz')
            feller = ha6-(ha6-ha5)**2/(ha6 - 2*ha5 + haq)
            return feller
    elif dimer == 'cyan' or dimer == 'p2' or dimer == 'pccp':
        if method == 'hf':
            energies = basis_energies(data, dimer, config, dist, method)
            a6, a5, aq = energies('a6z', 'a5z', 'aqz')
            feller = a6-(a6-a5)**2/(a6 - 2*a5 + aq)
            return feller
   

    

        
def get_extrapolation(data):
    pass



def get_data(indir='/home/marjory/chem'):
    data = {}
    methods = {
        'hf': ehf,
        'mp2': emp2,
        'ccsd': eccsd,
        'ccsdt': eccsdt,
    }
    def default_method_dict():
        return {name: {} for name,func in methods.items() }

    
    for root,dirs,filenames in os.walk(indir):
        #name = os.path.join(root,'scan.out')
        for f in filenames:
            if fnmatch.fnmatch(f,'*scan.out'):
                name = os.path.join(root,f)
                name = name.strip()
                #print name
                col = name.split("/")
                dimer, config, basis, dist = col[-5:-1]# , col[5], col[6], col[7]
                if not dimer in {'acet', 'diac', 'cyan', 'p2', 'pccp'}:
                    continue
                dimer_dict = data.setdefault(dimer,{})
                config_dict = dimer_dict.setdefault(config, {})
                dist_dict = config_dict.setdefault(dist,default_method_dict())
                newname = str(dimer + config + dist)
                #print newname
                with open(name,'r') as cat:

                    for line in cat:
                        for method_name, method_func in methods.items():
                            value = method_func(line)
                            if value is not None:
                                dist_dict[method_name][basis] = value
               
             
    return data

def all_corr_energies(data):
    for dimer in data:
        for config in data[dimer]:
            for dist in data[dimer][config]:
                all_bases = [set(data[dimer][config][dist][method].keys())
                             for method in data[dimer][config][dist]]
                shared = all_bases[0]
                for bases in all_bases[1:]:
                    shared = shared & bases
                for basis in shared:
                    mp2,ccsd,ccsdt = corr_energy(
                        data, dimer, config, dist, basis
                    )
                    yield dimer, config, dist, mp2, ccsd, ccsdt

def print_corr_energies(data):
    for dimer, config, dist, mp2, ccsd, ccsdt in all_corr_energies(data):
        print(dimer, config, dist, mp2, ccsd, ccsdt)
    
def main():     
    indir = '.'
    data = get_data(indir)
    print_corr_energies(data)
    #with open('data.json','wb') as wfp:
        #json.dump(data,wfp, indent=1)

    #for dimer in data:
        #for config in data[dimer]:
            #for dist in data[dimer][config]:
                #for method in data[dimer][config][dist]:
                    #print("blah")
                    #print(feller(data, dimer, config, dist, method))
    #print(corr_energy(data, 'acet', 'pd', '2.6', 'ha5z'))
    #print(feller(data, 'acet', 'pd', '2.6','hf'))
    #pprint.pprint(data)

if __name__=='__main__':
        
    main()
