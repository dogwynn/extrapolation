#!/usr/bin/env python3

import re
import os
import fnmatch
import pprint
import csv
import argparse
import collections
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def value(line):
    line = line.strip()
    line = line.split()
    retval = float(line[len(line)-1])

    return retval

def ehf(line):
    returned_value = None
    if re.search(r'!RHF STATE 1\.1 ENERGY',line):
        returned_value = value(line)

    elif re.search(r'!RHF STATE 1\.1 Energy',line):
        returned_value = value(line)

    return returned_value

def emp2(line):
    returned_value = None
    if re.search(r'MP2 STATE 1\.1 ENERGY',line):
        returned_value = value(line)

    elif re.search(r'!MP2 total energy',line):
        returned_value = value(line)

    elif re.search(r'!MP2 Energy',line):
        returned_value = value(line)

    elif re.search(r'MP2 total energy',line):
        returned_value = value(line) 

    return returned_value


def eccsd(line):
    returned_value = None
    if re.search(r'!CCSD STATE 1\.1 ENERGY',line):
        returned_value = value(line)
        
    elif re.search(r'CCSD total energy',line):
        returned_value = value(line)
        
    elif re.search(r'CCSD Energy',line):
        returned_value = value(line)
        
    return returned_value

def eccsdt(line):
    returned_value = None
    if re.search(r'!CCSD\(T\)',line):
        returned_value = value(line)
        
    return returned_value


def get_data(indir):
    dimers = []
    configs = []
    dists = []
    bases = []
    hf = []
    mp2 = []
    ccsd = []
    ccsd_t = []

    # A dictionary linking each method (a key) to the corresponding
    # energy function and the name of the method (the values)
    methods = {    
        'hf': (ehf, hf),
        'mp2': (emp2, mp2),
        'ccsd': (eccsd, ccsd),
        'ccsdt': (eccsdt, ccsd_t),
    }


    for root, dirs, filenames in os.walk(indir):
        for f in filenames:
            if fnmatch.fnmatch(f,'scan.out'):
                path = str(root) + '/' + str(f)
                #pprint.pprint(path)
                dimer = path.split('/')[-5]
                # If the 5th field from the end of the path is not one
                # of the dimers, this particular path will be
                # abandoned and the for loop will continue on to the
                # next f in filenames
                if not dimer in {'acet', 'diac', 'cyan', 'p2', 'pccp'}:  
                    continue

                config,basis,dist = path.split('/')[-4:-1]

                # Only want floating-point distances
                try:
                    dist = float(dist)
                except ValueError:
                    continue

                # Only want a and ha basis sets
                if not (basis.startswith('ha') or basis.startswith('a')):
                    continue
                
                dimers.append(dimer)
                configs.append(config)
                bases.append(basis)
                dists.append(dist)

                # Creates a lookup table for all the methods in
                # this file
                mdict = {}

                # Opens path so that the file can be read
                with open(path) as rfp:
                    # Iterates over each line in the current file
                    for line in rfp:
                        # Iterates over the keys in the methods
                        # dictionary, assigning the name 'method_func'
                        # to the energy parsing function associated
                        # with a particular method key and the name
                        # 'mlist' to the list of parsed values from
                        # each individual file
                        for method, (method_func, mlist) in methods.items():
                            # Creates a new variable, 'value', that is
                            # set equal to the output of the current
                            # energy function acting on the current
                            # line
                            value = method_func(line)
                            # Ensures that the variable 'value' above
                            # is actually equal to one of the method
                            # energies
                            # 
                            # If value is None, then no method energy
                            # was found
                            if value is not None:
                                # If a method energy was found, the
                                # method name key and method energy
                                # value are added to the dictionary
                                # 'mdict' as a key-value pair
                                mdict[method] = value

                # We have two possible outcomes after parsing the
                # file: either all of the methods have been found in
                # the file or some subset of them (possibly empty)
                # have been found
                #
                # This first loop appends the values we did find after
                # parsing the file
                for method,value in mdict.items(): 
                    _, mlist = methods[method]  #Assigns the 
                    mlist.append(value)

                # The second loop appends None for 
                for method in set(methods) - set(mdict):
                    _, mlist = methods[method]
                    mlist.append(None)

    # for mfunc,mlist in methods.values():
    #     print(mfunc,len(mlist),len(bases))




    columns = [('dimer',dimers), ('config',configs), ('dist', dists),
               ('basis', bases), ('hf', hf), ('mp2', mp2),
               ('ccsd', ccsd), ('ccsd_t', ccsd_t)]

    # A lambda expression is a "function one-liner"
    #
    # https://docs.python.org/3/tutorial/controlflow.html#lambda-expressions
    # 
    # For example:
    #
    # f = lambda x: x**2
    # f(2) == 4
    # f(3) == 9
    #
    # I.e. f is a function object that can be called like any function.
    #
    # The purpose of lambda functions are to be able to define
    # functional behavior as needed, especially when the function is
    # relatively simple (they are limited to a single expression).
    #
    # Below, we are using a lambda function to "wrap" the pd.Series
    # constructor so that we can map this constructor to the above
    # columns variable, a data structure that is composed of
    # (column_name, column_data) tuples. The output of the next line
    # is a list of pd.Series instances.
    series_columns = map( lambda tup: pd.Series(tup[1],name=tup[0]), columns )

    # If the creation of the pd.Series objects gets more complicated,
    # then we might want to replace the lambda expression with an
    # actual function, but since it's pretty simple, we'll stick with
    # the lambda for now.

    # Now we just pass the list of series objects to the concat
    # function.
    df = pd.concat(series_columns, axis=1)
    return df

def cor_column(method):
    return '{}_cor'.format(method)

def corr(dataframe):
    mp2_cor = pd.Series(dataframe['mp2'] - dataframe['hf'],
                        name=cor_column('mp2'))
    ccsd_cor = pd.Series(dataframe['ccsd'] - dataframe['hf'],
                         name = cor_column('ccsd'))
    ccsd_t_cor = pd.Series(dataframe['ccsd_t'] - dataframe['hf'],
                           name = cor_column('ccsd_t'))
    df = pd.concat([dataframe, mp2_cor, ccsd_cor, ccsd_t_cor], axis = 1)
    return df

def separate_basis(dataframe):
    def chi_map(basis):
        # Create a map of string to an integer where we will use the
        # last two characters of basis to find the integer equivalent
        # for that string
        cm = {'dz': 2, 'tz': 3, 'qz': 4, '5z': 5, '6z': 6}
        last_two = basis[-2:]
        return cm.get(last_two)

    chi = pd.Series(dataframe.basis.apply(chi_map), name='chi')

    return pd.concat([dataframe, chi], axis=1)
    # Perform similar action to make series with 'h' 
    # ditto with 'd' or 't' etc

def heavy(dataframe):
    def h_map(basis):
        # Let's keep it simple here
        return basis.startswith('h')

    heavy = pd.Series(dataframe.basis.apply(h_map), name = 'heavy')
    # FYI, you could replace the above h_map function (in the apply
    # method) with a lambda expression: e.g.
    # 
    # heavy = pd.Series(
    #      dataframe.basis.apply(lambda basis:basis.startswith('h')), 
    #      name = 'heavy'
    # )

    return pd.concat([dataframe,heavy], axis = 1)

def group_values(group, method, chi, heavy):
    '''Return the basis correlation values for a particular chi value (and
    heavy) as a numpy array

    group: DataFrame (e.g. returned by df.groupby.get_group)
       corresponding to a particular dimer, configuration and distance

    method: method correlation (str: e.g. 'mp2')

    chi: chi value (an integer)

    heavy: whether or not we want the heavy version of the molecule

    '''
    cor = cor_column(method)
    if method == 'hf':
        cor = 'hf'
        
    cor_values = group[(group.chi==chi) & (group.heavy==heavy)][cor]
    return cor_values.as_matrix()

def eq4_extrapolations(group, method, chi, heavy):
    chim1 = chi - 1
    chim2 = chi - 2

    chi_values = group_values(group, method, chi, heavy)
    chim1_values = group_values(group, method, chim1, heavy)
    chim2_values = group_values(group, method, chim2, heavy)

    extrap = chi_values - ( ( (chi_values - chim1_values)**2 ) /
                            ( chi_values - 2*chim1_values + chim2_values ) )
    return extrap

def eq6_extrapolations(group, method, chi, heavy):
    chim1 = chi - 1

    chi_values = group_values(group, method, chi, heavy)
    chim1_values = group_values(group, method, chim1, heavy)

    chi3 = chi**3
    chim1_3 = chim1**3

    extrap = ( ((chi3 * chi_values) - (chim1_3 * chim1_values)) /
               (chi3 - chim1_3) )

    return extrap

def eq7_extrapolations(group, method, chi, heavy):
    chim1 = chi - 1

    chi_values = group_values(group, method, chi, heavy)
    chim1_values = group_values(group, method, chim1, heavy)

    chip5_3 = (chi+0.5)**3
    chim5_3 = (chi-0.5)**3

    extrap = ( ( (chip5_3 * chi_values) - (chim5_3 * chim1_values) ) /
               (chip5_3 - chim5_3) )

    return extrap

def hf_extrapolations(method, groups):
    data = []
    for (dimer, config, dist), group in groups:
        min_chi, max_chi = group.chi.min() + 2, group.chi.max()

        heavy = False
        if dimer in {'acet','diac'}:
            heavy = True

        extrapolations = {}
        for chi in range(group.chi.min(), group.chi.max()+1):
            extrap = eq4_extrapolations(group, method, chi, heavy)
            extrapolations[chi] = extrap.mean()

        elist = [extrapolations[chi] for chi in range(min_chi,max_chi+1)]

        data.append([dimer, config, dist] + elist)

    return pd.DataFrame(
        data, columns=['dimer','config','dist']+list(range(min_chi,max_chi+1))
    )
        
def non_hf_extrapolations(method, groups):
    data = []
    for (dimer, config, dist), group in groups:
        min_chi, max_chi = group.chi.min() + 1, group.chi.max()

        heavy = False
        if dimer in {'acet','diac'}:
            heavy = True

        extrapolations = {}
        for chi in range(min_chi, min(max_chi+1,6)):
            extrap = eq6_extrapolations(group, method, chi, heavy)
            extrapolations[chi] = extrap.mean()

        if max_chi >= 6:
            for chi in range(6, max_chi+1):
                extrap = eq7_extrapolations(group, method, chi, heavy)
                extrapolations[chi] = extrap.mean()

        elist = [extrapolations[chi] for chi in range(min_chi,max_chi+1)]

        data.append([dimer, config, dist] + elist)
    return pd.DataFrame(
        data, columns=['dimer','config','dist']+list(range(min_chi,max_chi+1))
    )
        
        

def all_extrapolations(df):
    grouping = df.groupby(['dimer','config','dist'])
    group_keys = sorted(grouping.groups.keys())
    groups = [(key, grouping.get_group(key)) for key in group_keys]

    method_extrap_funcs = {
        'hf': hf_extrapolations,
        'mp2': non_hf_extrapolations,
        'ccsd': non_hf_extrapolations,
        'ccsd_t': non_hf_extrapolations,
    }

    extrapolations = {}
    for method, method_extrap_func in method_extrap_funcs.items():
        extrapolations[method] = method_extrap_func(method, groups)

    return extrapolations

def combined_data(path):
    return separate_basis(
        heavy(
            corr(
                get_data(path)
            )
        )
    )

def get_args():
    # Defines an instance of the ArgumentParser class 
    parser = argparse.ArgumentParser(                       
        description="Arguments for extrapolations module",
    )

    # adds arguments to parser, taking the arguments from the input
    # (which is '/home/marjory/chem/data' by default)
    parser.add_argument('-i','--input',                     
                        default='/home/marjory/chem/data')


    # defines a new variable, 'args', which is the out put of the
    # parsing of parser
    args = parser.parse_args()              
    return args
    
def main():
    # calling get_args to get the command-line arguments passed to
    # Python
    args = get_args()
    
    df = combined_data(args.input)

    extrap = all_extrapolations(df)

    ccsd = extrap['ccsd']
    
    
    
if __name__=='__main__':
    main()


